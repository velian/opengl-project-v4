--require "scripts.init"

local running = true;

function string:split(sep)
        local sep, fields = sep or ":", {}
        local pattern = string.format("([^%s]+)", sep)
        self:gsub(pattern, function(c) fields[#fields+1] = c end)
        return fields
end

--This is all currently called each frame.
function main()

	if app.keyDown(string.byte("A")) then
		AddRigidbody(1);
	end

	

	--for i = 0, 10 do
	--	app.drawSquare(i * 2, i * 2, i * 2);
	--	app.drawSphere(i * 2, i * 2, i * 2);
	--end
	--if app.keyDown(string.byte("A")) then
	--	io.write("Hello m80\n");
	--end

	--if app.keyDown(string.byte("D")) then
	--	io.write("Negative Hello m80\n");
	--end
end

function PlaySound(sound)
	local vars = sound:split(" ");
	app.playSound(sound);
	io.write("Played sound : "..sound);
end

function AddRigidbody(type)
	app.log("Hello Wur;d");
	app.addRigidbody(physicsScene, type);
end

function SetTimestep(timestep)
	app.setTimestep(physicsScene, timestep);
end

local ConsoleCommands = {
	["PlaySound"] = PlaySound,
	["Main"] = main,
	["AddRigidbody"] = AddRigidbody,
	["SetTimestep"] = SetTimestep,
	--Other functions here
}

function HandleConsoleCommands(command, other)
	if ConsoleCommands[command] == nil then
		io.write("Invalid console command '" .. command .. "'");
		return;
	end

	ConsoleCommands[command](other);
end