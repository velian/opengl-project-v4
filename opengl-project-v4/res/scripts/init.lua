--app.log("Loaded INIT.LUA");

-- Default the scenes to uninitialized --
initInitialized = false;
mainInitialized = false;

-- Called once from the main() function in main.lua --
local function Initialize()
	--app.log("Initializing...");

	initInitialized = true;

	--app.playSound("Music", true);
end

if initInitialized == false then
	Initialize();
	--app.log("Initialized.");
end