#include <GLFW\glfw3.h>
#include <GL\gl_core_4_4.h>

#include "State/CustomPhysics.h"
#include "Entity/PhysicsScene.h"
#include "Entity/Sphere.h"
#include "Entity/Plane.h"
#include "Entity/SpringJoint.h"
#include "Entity/Cube.h"
#include "Entity/Camera.h"

#include "GLM/ext.hpp"

#include "Utility/SoundManager.h"
#include "Utility/Lua/LuaScript.h"

CustomPhysics::CustomPhysics(unsigned int _id, GameStateManager* _gameStateManager) : GameState(_id, _gameStateManager)
{
	m_physicsScene = nullptr;
	m_camera = nullptr;
}

void CustomPhysics::Initialize()
{
	//Set up all the physics objects
	m_physicsScene = new PhysicsScene();
	m_physicsScene->m_gravity = glm::vec3(0, -50, 0);

	m_physicsScene->AddRigidbody(new Plane());
	m_physicsScene->AddRigidbody(new Plane(glm::vec3( 1, 0, 0)));
	m_physicsScene->AddRigidbody(new Plane(glm::vec3(-1, 0, 0)));

	m_camera = new Camera(3.14159f * 0.25f, 4.0f / 3.0f, 0.01f, 100.f);
	m_camera->setSpeed(0);
	m_camera->setLookAtFrom(glm::vec3(0, 5, -20), glm::vec3(0, 0, 0));

	SoundManager::Instance()->LoadSound("./sounds/music.mp3", "Music");
	SoundManager::Instance()->LoadSound("./sounds/collision.wav", "Collision");	

	m_luaMain = new LuaScript("./scripts/main.lua");

	lua_pushlightuserdata(m_luaMain->m_luaState, this);
	lua_setglobal(m_luaMain->m_luaState, "physicsScene");
}

void CustomPhysics::Update(double _deltaTime)
{
	m_physicsScene->Update(_deltaTime);
	m_camera->update((float)_deltaTime);

	m_luaMain->RunFile();
	m_luaMain->RunFunction("main", "");

	if(ImGui::GetConsoleUpdated())
	{
		const char* command_line = ImGui::GetConsoleBuffer();
		std::string strCommandLine(command_line);

		if (strCommandLine[0] != '/') return;
		
		size_t uiSpaceLoc = 0;

		//Find first space location
		if (strCommandLine.find(' ') != std::string::npos)
		{
			uiSpaceLoc = strCommandLine.find(' ');
		}
		
		std::string command;
		std::string vars;

		if (uiSpaceLoc != 0)
		{
			command = strCommandLine.substr(1, uiSpaceLoc - 1);
			vars = strCommandLine.substr(uiSpaceLoc + 1, strCommandLine.length());
		}
		else
		{
			command = strCommandLine.substr(1, strCommandLine.length());
		}
		
		lua_getglobal(m_luaMain->m_luaState, "HandleConsoleCommands");
		lua_pushstring(m_luaMain->m_luaState, command.c_str());
		
		if (!vars.empty())
			lua_pushstring(m_luaMain->m_luaState, vars.c_str());

		lua_pcall(m_luaMain->m_luaState, (vars.empty()) ? 1 : 2, 0, 0);

	}

	//remove spheres on middle click
	if (glfwGetKey(glfwGetCurrentContext(), GLFW_KEY_LEFT_CONTROL) && ImGui::IsMouseClicked(0))
	{
		double mouseX, mouseY;
		glfwGetCursorPos(glfwGetCurrentContext(), &mouseX, &mouseY);
		glm::vec3 position = m_camera->pickAgainstPlane(mouseX, mouseY, glm::vec4(0, 0, 1, 0));

		m_physicsScene->AddRigidbody(new Cube(position));
	}
	//Spawn a nonstatic spring on left click and shift
	else if (glfwGetKey(glfwGetCurrentContext(), GLFW_KEY_LEFT_SHIFT) && ImGui::IsMouseClicked(0))
	{
		double mouseX, mouseY;
		glfwGetCursorPos(glfwGetCurrentContext(), &mouseX, &mouseY);
		glm::vec3 position = m_camera->pickAgainstPlane(mouseX, mouseY, glm::vec4(0, 0, 1, 0));
		Sphere* sphere1 = new Sphere(position, false);
		Sphere* sphere2 = new Sphere(position + glm::vec3(0, 5, 0), false);

		m_physicsScene->AddRigidbody(new SpringJoint(sphere1, sphere2));
	}
	//Spawn a sphere on left click - nonstatic
	else if (ImGui::IsMouseClicked(0))
	{
		double mouseX, mouseY;
		glfwGetCursorPos(glfwGetCurrentContext(), &mouseX, &mouseY);
		glm::vec3 position = m_camera->pickAgainstPlane(mouseX, mouseY, glm::vec4(0,0,1,0));
		m_physicsScene->AddRigidbody(new Sphere(position));
	}	

	//Spawn a static spring on right click and shift
	if (glfwGetKey(glfwGetCurrentContext(), GLFW_KEY_LEFT_SHIFT) && ImGui::IsMouseClicked(1))
	{
		double mouseX, mouseY;
		glfwGetCursorPos(glfwGetCurrentContext(), &mouseX, &mouseY);
		glm::vec3 position = m_camera->pickAgainstPlane(mouseX, mouseY, glm::vec4(0, 0, 1, 0));
		Sphere* sphere1 = new Sphere(position, false);
		Sphere* sphere2 = new Sphere(position + glm::vec3(0, 5, 0), true);

		m_physicsScene->AddRigidbody(new SpringJoint(sphere1, sphere2));
	}
	//Spawn a sphere on right click - static
	else if (ImGui::IsMouseClicked(1))
	{
		double mouseX, mouseY;
		glfwGetCursorPos(glfwGetCurrentContext(), &mouseX, &mouseY);
		glm::vec3 position = m_camera->pickAgainstPlane(mouseX, mouseY, glm::vec4(0, 0, 1, 0));
		m_physicsScene->AddRigidbody(new Sphere(position, true, 1, glm::vec4(1,0,0,1)));
	}

	//remove all on middle click
	if (ImGui::IsMouseClicked(2))
	{
		m_physicsScene->m_rigidbodies.clear();
		m_physicsScene->AddRigidbody(new Plane());
		m_physicsScene->AddRigidbody(new Plane(glm::vec3(1, 0, 0)));
		m_physicsScene->AddRigidbody(new Plane(glm::vec3(-1, 0, 0)));
	}	

	if (glfwGetKey(glfwGetCurrentContext(), GLFW_KEY_F5))
	{
		m_luaMain->Reload();
	}
}

void CustomPhysics::Draw()
{
	DrawGUI("Custom Physics");
	m_physicsScene->Debug();	

	//ImGui::Text("Camera Position - %f, %f, %f", m_camera->getTransform()[3].x, m_camera->getTransform()[3].y, m_camera->getTransform()[3].z);

	//Gizmos::addSphere(glm::vec3(0), 1, 16, 16, glm::vec4(1));

	Gizmos::draw(m_camera->getProjectionView());
	Gizmos::draw2D(m_camera->getProjectionView());
}

void CustomPhysics::DrawGUI(char* _stateName)
{
	GameState::DrawGUI(_stateName);
	ImGui::ShowCustomConsole();
}

void CustomPhysics::AddRigidbody(ShapeID _shapeID)
{
	if (_shapeID == ShapeID::SPHERE)
	{
		m_physicsScene->AddRigidbody(new Sphere());
	}
}