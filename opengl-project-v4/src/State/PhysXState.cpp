#include <GLFW\glfw3.h>
#include <GL\gl_core_4_4.h>

#include "State/PhysXState.h"
#include "Entity/Camera.h"
#include "Entity/PhysXRagdoll.h"
#include "Entity/ParticleEmitter.h"
#include "Entity/ParticleFluidEmitter.h"

#include "GLM/ext.hpp"

#include "Utility/PhysXPlayerController.h"
#include "Utility/Gizmos.h"
#include "Utility/SoundManager.h"

PhysXState::PhysXState(unsigned int _id, GameStateManager* _gameStateManager) : GameState(_id, _gameStateManager)
{
	m_camera = nullptr;

	m_camera = new Camera(3.14159f * 0.25f, 4.0f / 3.0f, 0.01f, 100.f);
	m_camera->setSpeed(5);
	m_camera->setLookAtFrom(glm::vec3(0, 5, -20), glm::vec3(0, 0, 0));

	PxAllocatorCallback* m_callback = new MemAlloc();
	m_physicsFoundation = PxCreateFoundation(PX_PHYSICS_VERSION, *m_callback, m_defaultErrorCallback);
}

void PhysXState::Initialize()
{
	//Clear what we already had in the scene just in case.
	m_rigidDynamics.clear();
	m_rigidStatics.clear();
	m_ragdolls.clear();
	m_particleSystems.clear();

	//Initialize PhysX Scene	
	m_physics = PxCreatePhysics(PX_PHYSICS_VERSION, *m_physicsFoundation, PxTolerancesScale());
	PxInitExtensions(*m_physics);
	m_physicsMaterial = m_physics->createMaterial(0.1f, 0.1f, 0.1f);
	PxSceneDesc sceneDesc(m_physics->getTolerancesScale());
	sceneDesc.gravity = PxVec3(0, -30.0f, 0);
	sceneDesc.filterShader = &physx::PxDefaultSimulationFilterShader;
	sceneDesc.cpuDispatcher = PxDefaultCpuDispatcherCreate(1);
	m_physicsScene = m_physics->createScene(sceneDesc);

	//Aux Scene Setup (Timers and the like)
	m_bloodTimer = 1.f;

	//Plane Setup
	PxTransform pose = PxTransform(PxVec3(0, -5, 0), PxQuat(PxHalfPi * 1, PxVec3(0, 0, 1)));
	m_plane = PxCreateStatic(*m_physics, pose, PxPlaneGeometry(), *m_physicsMaterial);
	m_physicsScene->addActor(*m_plane);

	//Sphere setup code
	float density = 1;	
	PxSphereGeometry sphere(1);	
	for (int i = 0; i < 5; i++)
	{
		PxTransform sphereTransform(PxVec3(sin(i), i * 2 + 10, cos(i)));
		m_sphere = PxCreateDynamic(*m_physics, sphereTransform, sphere, *m_physicsMaterial, density);
		m_rigidDynamics.push_back(m_sphere);
	}	

	//Setup Cubes
	PxBoxGeometry box(1,1,1);
	for (int i = 0; i < 5; i++)
	{
		for (int x = 0; x < 5; x++)
		{
			PxRigidDynamic* boxObj = PxCreateDynamic(*m_physics, PxTransform(i * 4, 0, x * 4), box, *m_physicsMaterial, density);
			m_rigidDynamics.push_back(boxObj);
		}
	}

	//Setup trigger volume
	PxBoxGeometry triggerVol(2, 2, 2);
	m_trigger = PxCreateStatic(*m_physics, PxTransform(-12.5, -2.5, 0), triggerVol, *m_physicsMaterial);

	physx::PxShape** shapes = new PxShape*[m_trigger->getNbShapes()];
	m_trigger->getShapes(shapes, m_trigger->getNbShapes());
	shapes[0]->setFlag(PxShapeFlag::eSIMULATION_SHAPE, false);
	shapes[0]->setFlag(PxShapeFlag::eTRIGGER_SHAPE, true);
	delete[] shapes;

	m_triggerCallback = TriggerCallback();
	m_physicsScene->setSimulationEventCallback(&m_triggerCallback);

	m_rigidStatics.push_back(m_trigger);

	//Setup ragdoll
	m_ragdollPosition = new PxTransform(PxVec3(-5, 0, 0));
	m_ragdollArticulation = Ragdoll::makeRagdoll(m_physics, ragdollData, *m_ragdollPosition, .1f, m_physicsMaterial);
	m_ragdolls.push_back(m_ragdollArticulation);

	//Setup Player
	PxCapsuleControllerDesc* plyDesc = new PxCapsuleControllerDesc();
	m_playerController = new PlayerController();
	m_controllerManager = PxCreateControllerManager(*m_physicsScene);

	HitReport* hitReport = new HitReport();
	plyDesc->height = 2.f;
	plyDesc->radius = 0.5f;
	plyDesc->position.set(-5, -3.5, -2); 
	plyDesc->material = m_physicsMaterial;
	plyDesc->reportCallback = hitReport;
	plyDesc->density = density;

	m_playerController->Initialize(*m_physicsScene, plyDesc, m_controllerManager);

	//Setup Particle Systems
	PxU32 maxParticles = 1000;
	bool perParticleOffset = false;
	PxParticleFluid* m_particleSystem = m_physics->createParticleFluid(maxParticles, perParticleOffset);

	m_particleSystem->setRestParticleDistance(.3f);
	m_particleSystem->setDynamicFriction(0.1f);
	m_particleSystem->setStaticFriction(0.1f);
	m_particleSystem->setDamping(0.1);
	m_particleSystem->setParticleMass(.1);
	m_particleSystem->setRestitution(0.1);
	m_particleSystem->setParticleBaseFlag(PxParticleBaseFlag::eCOLLISION_TWOWAY, true);
	m_particleSystem->setStiffness(1);

	m_particleSystems.push_back(m_particleSystem);

	//Add all to the physXScene
	for (auto obj : m_rigidDynamics)
	{
		m_physicsScene->addActor(*obj);
	}

	for (auto obj : m_rigidStatics)
	{
		m_physicsScene->addActor(*obj);
	}

	for (auto ragdoll : m_ragdolls)
	{
		m_physicsScene->addArticulation(*ragdoll);
	}

	for (auto emitter : m_particleSystems)
	{
		m_physicsScene->addActor(*emitter);
		m_emitter = new ParticleFluidEmitter(maxParticles, m_ragdollPosition->p, emitter, 0.01);
		m_emitter->setStartVelocityRange(-1000.f, 100, -1000.f, 1000.f, 200.f, 1000.f);
	}
}

void PhysXState::Update(double _deltaTime)
{
	m_camera->update((float)_deltaTime);

	if (_deltaTime <= 0) return;

	m_physicsScene->simulate(_deltaTime);

	while (m_physicsScene->fetchResults() == false)
	{
		//Fetch Results
	}
	
	//Aux Update
	if(m_triggerCallback.isTriggered) m_bloodTimer -= _deltaTime;

	m_playerController->Update(_deltaTime, m_physicsScene->getGravity());

	if (m_emitter && m_triggerCallback.isTriggered)
	{
		if (m_bloodTimer > 0) return;

		m_emitter->update((float)_deltaTime);

		PxU32 nLinks = m_ragdollArticulation->getNbLinks();
		PxArticulationLink** links = new PxArticulationLink*[nLinks];
		m_ragdollArticulation->getLinks(links, nLinks);

		while (nLinks--)
		{
			PxArticulationLink* link = links[nLinks];
			PxU32 nShapes = link->getNbShapes();
			PxShape** shapes = new PxShape*[nShapes];
			link->getShapes(shapes, nShapes);
			while (nShapes--)
			{
				if (nLinks == 10)
				{
					PxTransform transform = PxShapeExt::getGlobalPose(*shapes[nShapes], *link);

					m_emitter->m_position = transform.p + PxVec3(0,0.25,0);
				}
			}
		}
	}

	if (ImGui::IsKeyPressed(GLFW_KEY_F5, false))
	{
		//Set all positions to defaults
		m_physicsScene->release();
		m_physics->release();

		Initialize();
	}
}

void PhysXState::Draw()
{
	DrawGUI("PhysX Scene");

	for (auto rigid : m_rigidDynamics)
	{
		PxU32 nShapes = rigid->getNbShapes();
		PxShape** shapes = new PxShape*[nShapes];
		rigid->getShapes(shapes, nShapes);

		while (nShapes--)
		{
			AddWidget(shapes[nShapes], rigid);
		}
	}

	for (auto rigid : m_rigidStatics)
	{
		PxU32 nShapes = rigid->getNbShapes();
		PxShape** shapes = new PxShape*[nShapes];
		rigid->getShapes(shapes, nShapes);

		while (nShapes--)
		{
			AddWidget(shapes[nShapes], rigid, !m_triggerCallback.isTriggered ? glm::vec4(0, 1, 0, 1) : glm::vec4(1, 0, 1, 1));
		}
	}

	for (auto articulation : m_ragdolls)
	{
		PxU32 nLinks = articulation->getNbLinks();
		PxArticulationLink** links = new PxArticulationLink*[nLinks];
		articulation->getLinks(links, nLinks);

		while (nLinks--)
		{
			PxArticulationLink* link = links[nLinks];
			PxU32 nShapes = link->getNbShapes();
			PxShape** shapes = new PxShape*[nShapes];
			link->getShapes(shapes, nShapes);
			while (nShapes--)
			{
				AddWidget(shapes[nShapes], link, glm::vec4(0.1 * nLinks, 0.1 * nLinks, 0.1 * nLinks, 1));
			}
		}
		delete[] links;
	}

	if (m_playerController)
	{
		PxU32 nShapes = m_controllerManager->getController(0)->getActor()->getNbShapes();
		PxShape** shapes = new PxShape*[nShapes];
		m_controllerManager->getController(0)->getActor()->getShapes(shapes, nShapes);

		while (nShapes--)
		{
			AddWidget(shapes[nShapes], m_controllerManager->getController(0)->getActor());
		}
	}

	if (m_emitter)
	{
		if (m_bloodTimer < 0)
			m_emitter->renderParticles();
	}

	Gizmos::draw(m_camera->getProjectionView());
	Gizmos::draw2D(m_camera->getProjectionView());
}

void PhysXState::DrawGUI(char* _stateName)
{
	GameState::DrawGUI(_stateName);
	//ImGui::ShowCustomConsole();
}

void PhysXState::AddWidget(PxShape* _shape, PxRigidActor* _actor, glm::vec4 _colour)
{
	PxGeometryType::Enum type = _shape->getGeometryType();

	switch (type)
	{
	case PxGeometryType::ePLANE:
		AddPlane(_shape, _actor, _colour);
		break;
	case PxGeometryType::eBOX:
		AddBox(_shape, _actor, _colour);
		break;
	case PxGeometryType::eSPHERE:
		AddSphere(_shape, _actor, _colour);
		break;
	case PxGeometryType::eCAPSULE:
		AddCapsule(_shape, _actor, _colour);
		break;
	default:
		//AddBox(_shape, _actor, _colour);
		//AddSphere(_shape, _actor);
		break;
	}
}

void PhysXState::AddPlane(PxShape* _shape, PxRigidActor* _actor, glm::vec4 _colour)
{
	//get the geometry for this PhysX collision volume
	PxBoxGeometry geometry;
	float width = 1, height = 1, length = 1;
	bool status = _shape->getBoxGeometry(geometry);
	if (status)
	{
		width = geometry.halfExtents.x;
		height = geometry.halfExtents.y;
		length = geometry.halfExtents.z;
	}
	//get the transform for this PhysX collision volume
	PxMat44 m(PxShapeExt::getGlobalPose(*_shape, *_actor));
	glm::mat4 M(m.column0.x, m.column0.y, m.column0.z, m.column0.w,
		m.column1.x, m.column1.y, m.column1.z, m.column1.w,
		m.column2.x, m.column2.y, m.column2.z, m.column2.w,
		m.column3.x, m.column3.y, m.column3.z, m.column3.w);
	glm::vec3 position;
	//get the position out of the transform
	position.x = m.getPosition().x;
	position.y = m.getPosition().y;
	position.z = m.getPosition().z;
	glm::vec3 extents = glm::vec3(width, height, length);

	glm::vec4 colour = (_colour == glm::vec4(1)) ? glm::vec4(1, 0, 0, 1) : _colour;

	//create our box gizmo
	Gizmos::addAABBFilled(position, extents, colour, &M);
}

void PhysXState::AddBox(PxShape* _shape, PxRigidActor* _actor, glm::vec4 _colour)
{
	//get the geometry for this PhysX collision volume
	PxBoxGeometry geometry;
	float width = 1, height = 1, length = 1;
	bool status = _shape->getBoxGeometry(geometry);
	if (status)
	{
		width = geometry.halfExtents.x;
		height = geometry.halfExtents.y;
		length = geometry.halfExtents.z;
	}
	//get the transform for this PhysX collision volume
	PxMat44 m(PxShapeExt::getGlobalPose(*_shape, *_actor));
	glm::mat4 M(m.column0.x, m.column0.y, m.column0.z, m.column0.w,
		m.column1.x, m.column1.y, m.column1.z, m.column1.w,
		m.column2.x, m.column2.y, m.column2.z, m.column2.w,
		m.column3.x, m.column3.y, m.column3.z, m.column3.w);
	glm::vec3 position;
	//get the position out of the transform
	position.x = m.getPosition().x;
	position.y = m.getPosition().y;
	position.z = m.getPosition().z;
	glm::vec3 extents = glm::vec3(width, height, length);
	
	glm::vec4 colour = (_colour == glm::vec4(1)) ? glm::vec4(0.5, 0.5, 0.5, 1) : _colour;

	//create our box gizmo
	Gizmos::addAABBFilled(position, extents, colour, &M);
}

void PhysXState::AddSphere(PxShape* _shape, PxRigidActor* _actor, glm::vec4 _colour)
{
	//get the geometry for this PhysX collision volume
	PxSphereGeometry geometry;
	float radius = 1;
	bool status = _shape->getSphereGeometry(geometry);
	if (status)
	{
		radius = geometry.radius;
	}
	//get the transform for this PhysX collision volume
	PxMat44 m(PxShapeExt::getGlobalPose(*_shape, *_actor));
	glm::mat4 M(m.column0.x, m.column0.y, m.column0.z, m.column0.w,
		m.column1.x, m.column1.y, m.column1.z, m.column1.w,
		m.column2.x, m.column2.y, m.column2.z, m.column2.w,
		m.column3.x, m.column3.y, m.column3.z, m.column3.w);
	glm::vec3 position;
	//get the position out of the transform
	position.x = m.getPosition().x;
	position.y = m.getPosition().y;
	position.z = m.getPosition().z;
	glm::vec4 colour = (_colour == glm::vec4(1)) ? glm::vec4(1, 1, 1, 1) : _colour;
	//create our sphere gizmo
	Gizmos::addSphere(position, radius, 16, 16, colour, &M);
}

void PhysXState::AddCapsule(PxShape* _shape, PxRigidActor* _actor, glm::vec4 _colour)
{
	//get the geometry for this PhysX collision volume
	PxCapsuleGeometry geometry;
	float radius = 1;
	float halfHeight = 0;

	bool status = _shape->getCapsuleGeometry(geometry);
	if (status)
	{
		radius = geometry.radius; //copy out capsule radius
		halfHeight = geometry.halfHeight; //copy out capsule half length
	}
	PxTransform transform = PxShapeExt::getGlobalPose(*_shape, *_actor);

	glm::vec4 colour = (_colour == glm::vec4(1)) ? glm::vec4(1, 1, 1, 1) : _colour;

	PxMat44 m(transform); //Create a rotation matrix from transform
	glm::mat4* M = (glm::mat4*)(&m);
	//get the world position from the PhysX tranform
	glm::vec3 position = glm::vec3(transform.p.x, transform.p.y, transform.p.z);
	glm::vec4 axis(halfHeight, 0, 0, 0); //axis for the capsule
	axis = *M * axis; //rotate axis to correct orientation

	//add our 2 end cap spheres...
	Gizmos::addSphere(position + axis.xyz(), radius, 10, 10, colour);
	Gizmos::addSphere(position - axis.xyz(), radius, 10, 10, colour);

	//Fix the gizmo rotation
	glm::mat4 m2 = glm::rotate(*M, 11 / 7.0f, glm::vec3(0.0f, 0.0f, 1.0f));
	Gizmos::addCylinderFilled(position, radius, halfHeight, 10, colour, &m2);

	
	//create our sphere gizmo
	//Gizmos::addSphere(position, radius, 16, 16, colour, &M);
}