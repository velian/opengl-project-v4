#include <IMGUI/ImGui.h>

#include "Entity/PhysicsScene.h"
#include "Entity/SpringJoint.h"
#include "Entity/Sphere.h"
#include "Entity/Plane.h"
#include "Entity/Cube.h"

PhysicsScene::PhysicsScene()
{
	m_timestep = 1.f;
}

void PhysicsScene::AddRigidbody(Rigidbody* _rigidbody)
{
	m_rigidbodies.push_back(_rigidbody);
}

void PhysicsScene::RemoveRigidbody(Rigidbody* _rigidbody)
{	
	//Remove rigidbody at location
	for (unsigned int i = 0; i < m_rigidbodies.size(); i++)
	{
		if (m_rigidbodies[i] == _rigidbody)
		{
			m_rigidbodies.erase(m_rigidbodies.begin() + i);
			return;
		}
	}
}

void PhysicsScene::Update(double _deltaTime)
{
	for(Rigidbody* rigidbody : m_rigidbodies)
	{
		if (rigidbody->m_shapeID != 0)
			rigidbody->Update(m_gravity, (float)_deltaTime * m_timestep);
	}

	CheckCollisions();
}

void PhysicsScene::Debug()
{
	for each(Rigidbody* rigidbody in m_rigidbodies)
	{
		rigidbody->Debug();
	}
}

void PhysicsScene::CheckCollisions()
{
	int rigidbodyCount = m_rigidbodies.size();

	for (int outer = 0; outer < rigidbodyCount - 1; outer++)
	{
		for (int inner = outer + 1; inner < rigidbodyCount; inner++)
		{
			PhysicsObject* object1 = m_rigidbodies[outer];
			PhysicsObject* object2 = m_rigidbodies[inner];

			ShapeID _shapeID1 = object1->m_shapeID;
			ShapeID _shapeID2 = object2->m_shapeID;

			switch (_shapeID1)
			{
			case PLANE:
				switch (_shapeID2)
				{
				case PLANE:

					break;
				case SPHERE:
					SphereToPlane(object2, object1);
					break;
				case BOX:
					CubeToPlane(object2, object1);
					break;
				case JOINT:
					SpringJoint* spring = dynamic_cast<SpringJoint*>(object2);

					for each(Rigidbody* joint in spring->m_connections)
					{
						SphereToPlane(joint, object1);
					}

					break;
				}
				break; //Plane

			case BOX:

				switch (_shapeID2)
				{
				case PLANE:
					CubeToPlane(object1, object2);
					break;
				case SPHERE:
					SphereToCube(object2, object1);
					break;
				case BOX:
						//Box Box Collision
					break;
				case JOINT:
					SpringJoint* spring = dynamic_cast<SpringJoint*>(object2);

					for each(Rigidbody* joint in spring->m_connections)
					{
						SphereToPlane(joint, object1);
					}

					break;
				}

				break; //Box

			case SPHERE:
				switch (_shapeID2)
				{
				case PLANE:
					SphereToPlane(object1, object2);
					break;
				case SPHERE:
					SphereToSphere(object2, object1);
					break;
				case BOX:
					SphereToCube(object1, object2);
					break;
				case JOINT:
					SpringJoint* spring = dynamic_cast<SpringJoint*>(object2);

					for each(Rigidbody* joint in spring->m_connections)
					{
						SphereToSphere(joint, object1);
					}
					break;
				}
				break;//Sphere

			case JOINT:

				SpringJoint* spring = dynamic_cast<SpringJoint*>(object1);

				switch (_shapeID2)
				{
				case PLANE:
					for each(Rigidbody* joint in spring->m_connections)
					{
						SphereToPlane(joint, object2);
					}
					break;
				case SPHERE:
					for each(Rigidbody* joint in spring->m_connections)
					{
						SphereToSphere(joint, object2);
					}
					break;
				case BOX:
					for each(Rigidbody* joint in spring->m_connections)
					{
						SphereToCube(joint, object2);
					}
					break;
				case JOINT:
					SpringJoint* spring2 = dynamic_cast<SpringJoint*>(object2);

					for each(Rigidbody* joint1 in spring->m_connections)
					{
						for each(Rigidbody* joint2 in spring2->m_connections)
						{
							SphereToSphere(joint1, joint2);
						}
					}
					break;
				break;//Joint				
				}
			}
		}
	}
}

bool PhysicsScene::SphereToSphere(PhysicsObject* _rigid1, PhysicsObject* _rigid2)
{
	//Try to cast objects to sphere and sphere
	Sphere* sphere1 = dynamic_cast<Sphere*>(_rigid1);
	Sphere* sphere2 = dynamic_cast<Sphere*>(_rigid2);

	//If success
	if (sphere1 != NULL && sphere2 != NULL)
	{
		//Get the distance between the two spheres.
		glm::vec3 delta = sphere1->m_position - sphere2->m_position;
		float dist = glm::distance(sphere1->m_position, sphere2->m_position);

		//Make sure we interesected with them.
		float intersect = (sphere1->m_radius + sphere2->m_radius - dist);

		//If we DID intersect, collision code
		if (intersect > 0)
		{
			glm::vec3 collisionNormal = glm::normalize(delta);
			glm::vec3 relativeVelocity = sphere1->m_velocity - sphere2->m_velocity;
			glm::vec3 collisionVector = collisionNormal *(glm::dot(relativeVelocity, collisionNormal));
			glm::vec3 forceVector = collisionVector * 1.0f / (1 / sphere1->m_mass + 1 / sphere2->m_mass);

			//glm::vec3 fTotalMomentum = relativeVelocity * m_ball2.GetMass();
			//use newtons third law to apply colision forces to colliding bodies.
			

			//move balls out of collisions
			float fTotalMass = sphere1->m_mass + sphere2->m_mass;
			glm::vec3 seperationVector = collisionNormal * intersect;

			if (sphere1->m_static != true)
			{
				sphere1->AddForce(2.f * -forceVector);
				sphere1->m_position += (seperationVector * (sphere1->m_mass / fTotalMass));
			}
			if (sphere2->m_static != true)
			{
				sphere2->AddForce(2.f *  forceVector);
				sphere2->m_position += (-seperationVector * (sphere2->m_mass / fTotalMass));
			}			

			return true;
		}

		//return true;
	}
	return false;
}

bool PhysicsScene::SphereToPlane(PhysicsObject* _rigid1, PhysicsObject* _rigid2)
{
	//Try to cast objects to sphere and sphere
	Sphere* sphere = dynamic_cast<Sphere*>(_rigid1);
	Plane*	plane =	 dynamic_cast<Plane*> (_rigid2);

	//If success
	if (sphere != NULL && plane != NULL)
	{
		glm::vec3 collisionNormal = plane->m_normal;
		float sphereToPlane = glm::dot(sphere->m_position, plane->m_normal) - plane->m_distance;
		float intersection = sphere->m_radius - sphereToPlane;

		if (intersection > 0)
		{
			glm::vec3 planeNormal = collisionNormal;
			glm::vec3 relativeVelocity = sphere->m_velocity;
			glm::vec3 forceVector = -1 * sphere->m_mass * planeNormal * (glm::dot(planeNormal, sphere->m_velocity));

			sphere->AddForce(2.f * forceVector);
			sphere->m_position += collisionNormal * intersection * 0.5f;
			
			return true;
		}
	}
	return false;
}

bool PhysicsScene::SphereToCube(PhysicsObject* _rigid1, PhysicsObject* _rigid2)
{
	//Try to cast objects to sphere and sphere
	Sphere* sphere = dynamic_cast<Sphere*>(_rigid1);
	Cube*	cube   = dynamic_cast<Cube*  >(_rigid2);

	//If success
	if (sphere != NULL && cube != NULL)
	{
		cube->GenerateXAxis();
		cube->GenerateYAxis();
		cube->GenerateZAxis();

		float minimumSphere = sphere->m_position.x - sphere->m_radius;
		float maximumSphere = sphere->m_position.x + sphere->m_radius;

		float fIntersectDistance = 0;
		glm::vec3 collisionNormal = glm::vec3(0);

		if (minimumSphere < cube->m_xAxis[1] && maximumSphere > cube->m_xAxis[0])
		{
			minimumSphere = sphere->m_position.y - sphere->m_radius;
			maximumSphere = sphere->m_position.y + sphere->m_radius;

			float fXInterceptDistOne = minimumSphere - cube->m_xAxis[1];
			float fXInterceptDistTwo = maximumSphere - cube->m_xAxis[0];

			if (glm::abs(fXInterceptDistOne) < glm::abs(fXInterceptDistTwo))
			{
				fIntersectDistance = fXInterceptDistOne;
			}
			else
			{
				fIntersectDistance = fXInterceptDistTwo;
			}
			collisionNormal = glm::vec3(1, 0, 0);

			if (minimumSphere < cube->m_yAxis[1] && maximumSphere > cube->m_yAxis[0])
			{
				minimumSphere = sphere->m_position.y - sphere->m_radius;
				maximumSphere = sphere->m_position.y + sphere->m_radius;

				float fYInterceptDistOne = minimumSphere - cube->m_yAxis[1];
				float fYInterceptDistTwo = maximumSphere - cube->m_yAxis[0];

				float fBestYIntersectDist = 0;

				if (glm::abs(fYInterceptDistOne) < glm::abs(fYInterceptDistTwo))
				{
					fBestYIntersectDist = fYInterceptDistOne;
				}
				else
				{
					fBestYIntersectDist = fYInterceptDistTwo;
				}

				if (glm::abs(fBestYIntersectDist) < glm::abs(fIntersectDistance))
				{
					fIntersectDistance = fBestYIntersectDist;
					collisionNormal = glm::vec3(0, 1, 0);
				}

				//Generate force to shove both rigids
				glm::vec3 relativeVelocity = cube->m_velocity - sphere->m_velocity;
				glm::vec3 collisionVector = collisionNormal *(glm::dot(relativeVelocity, collisionNormal));
				glm::vec3 forceVector = collisionVector * 1.0f / (1 / cube->m_mass + 1 / sphere->m_mass);
				
				float fTotalMass = cube->m_mass + sphere->m_mass;
				glm::vec3 seperationVector = collisionNormal * fIntersectDistance;

				if (cube->m_static != true)
				{
					cube->AddForce(2.f * -forceVector);
					cube->m_position += (seperationVector * (cube->m_mass / fTotalMass));
				}
				if (sphere->m_static != true)
				{
					sphere->AddForce(2.f *  forceVector);
					sphere->m_position += (-seperationVector * (sphere->m_mass / fTotalMass));
				}

				return true;
			}
		}		
	}

	return false;
}

bool PhysicsScene::CubeToPlane(PhysicsObject* _rigid1, PhysicsObject* _rigid2)
{
	//Try to cast objects to sphere and sphere
	Cube*	cube = dynamic_cast<Cube*>(_rigid1);
	Plane*	plane = dynamic_cast<Plane*> (_rigid2);

	//If success
	if (cube != NULL && plane != NULL)
	{
		glm::vec3 collisionNormal = plane->m_normal;
		float sphereToPlane = glm::dot(cube->m_position, plane->m_normal) - plane->m_distance;
		float intersection = cube->m_extents - sphereToPlane;

		if (intersection > 0)
		{
			glm::vec3 relativeVelocity = cube->m_velocity;
			glm::vec3 planeNormal = collisionNormal;
			glm::vec3 forceVector = -1 * cube->m_mass * planeNormal * (glm::dot(planeNormal, cube->m_velocity));

			cube->AddForce(forceVector * 2.f);
			cube->m_position += collisionNormal * intersection;

			return true;
		}
	}
	return false;
}