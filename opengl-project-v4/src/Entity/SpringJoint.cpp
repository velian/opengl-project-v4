#include "Entity/SpringJoint.h"

SpringJoint::SpringJoint(Rigidbody* _first, Rigidbody* _second, float _springCoefficient, float _damping) : Rigidbody()
{
	m_connections[0] = _first, m_connections[1] = _second;
	m_springCoefficient = _springCoefficient;
	m_damping = _damping;
	m_restlength = glm::length(m_connections[0]->m_position - m_connections[1]->m_position);
	m_shapeID = JOINT;
	m_connections[0]->m_shapeID = SPHERE;
	m_connections[1]->m_shapeID = SPHERE;
}

void SpringJoint::Debug()
{
	Gizmos::addSphere(m_connections[0]->m_position, 1.f, 6, 6, glm::vec4(1, 0, 1, 1));
	Gizmos::addSphere(m_connections[1]->m_position, 1.f, 6, 6, glm::vec4(1, 1, 0, 1));

	Gizmos::addLine(m_connections[0]->m_position, m_connections[1]->m_position, glm::vec4(0, 1, 1, 1));
}

void SpringJoint::Update(glm::vec3 _gravity, float _deltaTime)
{
	glm::vec3 direction = (m_connections[0]->m_position - m_connections[1]->m_position);
	direction *= -m_springCoefficient - m_damping;

	if (glm::length(m_connections[0]->m_position - m_connections[1]->m_position) > m_restlength + m_damping)
	{
		m_connections[0]->AddForce(direction);
		m_connections[1]->AddForce(-direction);
	}
	else if (glm::length(m_connections[0]->m_position - m_connections[1]->m_position) < m_restlength - m_damping)
	{
		m_connections[0]->AddForce(-direction);
		m_connections[1]->AddForce(direction);
	}

	for (auto connection : m_connections)
	{
		connection->Update(_gravity, _deltaTime);
	}
}