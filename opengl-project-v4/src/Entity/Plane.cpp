#include "Entity/Plane.h"

Plane::Plane(glm::vec3 _normal, float _distance) : m_normal(_normal), m_distance(_distance)
{
	m_shapeID = ShapeID::PLANE;
}