#include "Entity/Rigidbody.h"
#include "IMGUI/imgui.h"

Rigidbody::Rigidbody(glm::vec3 _position, glm::vec3 _velocity, glm::vec2 _rotation, float _mass) :
m_position(_position), m_velocity(_velocity), m_rotation(_rotation), m_mass(_mass)
{
	m_drag = 15.0f;
}

Rigidbody::~Rigidbody()
{

}

void Rigidbody::Update(glm::vec3 _gravity, float _deltaTime)
{
	glm::vec3 normalVelocity(0);
	if (m_velocity != glm::vec3(0))
	{
		normalVelocity = glm::normalize(m_velocity);
	}

	m_velocity -= normalVelocity * m_drag * _deltaTime;

	m_velocity += _gravity * _deltaTime;
	m_position += m_velocity * _deltaTime;
}

void Rigidbody::Debug()
{
	ImGui::Text("Rigidbody Position : %f - %f - %f", m_position.x, m_position.y, m_position.z);
}

void Rigidbody::AddForce(glm::vec3 _force)
{
	m_velocity += _force / m_mass;
}