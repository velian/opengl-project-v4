#include "Utility\SoundManager.h"

SoundManager* SoundManager::m_instance = nullptr;

SoundManager* SoundManager::Instance()
{
	if (m_instance == nullptr)
	{
		m_instance = new SoundManager();
	}
	return m_instance;
}

SoundManager::SoundManager()
{
	FMOD::System_Create(&m_system);
	m_system->init(64, NULL, NULL);
}

void SoundManager::Update()
{
	m_system->update();
}

void SoundManager::PlayOneShot(std::string _soundName, bool _loop)
{
	for each(Sound* sound in m_soundList)
	{
		if (sound->m_name == _soundName)
		{
			sound->m_sound->setMode((_loop) ? FMOD_LOOP_NORMAL : FMOD_LOOP_OFF);
			m_system->playSound(sound->m_sound, NULL, false, NULL);
			return;
		}
	}
}

void SoundManager::StopSound(std::string _soundName)
{
	for each(Sound* sound in m_soundList)
	{
		if (sound->m_name == _soundName)
		{
			m_system->playSound(sound->m_sound, NULL, true, NULL);
			return;
		}
	}
}

void SoundManager::LoadSound(char* _filePath, char* _soundName)
{
	Sound* sound = new Sound(_filePath, _soundName);
	AddSound(sound);
}

void SoundManager::AddSound(Sound* _sound)
{
	m_soundList.push_back(_sound);
}