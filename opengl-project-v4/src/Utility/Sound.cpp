#include "Utility\SoundManager.h"
#include "Utility\Sound.h"

Sound::Sound(std::string _filePath, std::string _soundName)
{
	FMOD_RESULT result = SoundManager::Instance()->m_system->createSound(_filePath.c_str(), FMOD_DEFAULT, NULL, &m_sound);

	if (result == FMOD_OK)
	{
		m_name = _soundName;
		//Result was triggered
	}
	else if (result == FMOD_ERR_FILE_NOTFOUND)
	{
		printf("Sound not found - %s", _soundName);
	}
	else
	{
		printf("Error loading sound - %s", _soundName);
	}
}