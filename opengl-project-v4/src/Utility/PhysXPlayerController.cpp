#include "Utility/PhysXPlayerController.h"

#include <GLFW/glfw3.h>

void PlayerController::Initialize(PxScene& _scene, PxCapsuleControllerDesc* _desc, PxControllerManager* _manager)
{
	m_controller = _manager->createController(*_desc);
	_scene.addActor(*m_controller->getActor());
}

void PlayerController::Update(double _deltaTime, PxVec3 _gravity)
{
	PxControllerFilters filter;

	PxVec3 velocity(0);

	velocity += _gravity * _deltaTime;

	if (glfwGetKey(glfwGetCurrentContext(), GLFW_KEY_UP))
	{
		velocity.z += m_movementSpeed * _deltaTime;
	}
	if (glfwGetKey(glfwGetCurrentContext(), GLFW_KEY_DOWN))
	{
		velocity.z -= m_movementSpeed * _deltaTime;
	}
	if (glfwGetKey(glfwGetCurrentContext(), GLFW_KEY_LEFT))
	{
		velocity.x += m_movementSpeed * _deltaTime;
	}
	if (glfwGetKey(glfwGetCurrentContext(), GLFW_KEY_RIGHT))
	{
		velocity.x -= m_movementSpeed * _deltaTime;
	}
	if (glfwGetKey(glfwGetCurrentContext(), GLFW_KEY_E))
	{
		velocity.y += m_movementSpeed * _deltaTime;
	}
	if (glfwGetKey(glfwGetCurrentContext(), GLFW_KEY_C))
	{
		velocity.y -= m_movementSpeed * _deltaTime;
	}

	PxQuat rotation(m_rotation, PxVec3(0, 1, 0));

	m_controller->move(rotation.rotate(velocity), 0.001f, _deltaTime, filter);
}

void HitReport::onShapeHit(const PxControllerShapeHit &hit)
{
	PxRigidActor* actor = hit.shape->getActor();

	m_contactNormal = hit.worldNormal;

	//Attempt dynamic cast.
	PxRigidDynamic* myActor = actor->is<PxRigidDynamic>();
	if (myActor)
	{
		//Apply force to the thing we hit.
		myActor->addForce(m_contactNormal * 3);
	}
}

void HitReport::onControllerHit(const PxControllersHit &hit)
{
	
}

void HitReport::onObstacleHit(const PxControllerObstacleHit &hit)
{
		
}