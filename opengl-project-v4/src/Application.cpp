#include <GLFW\glfw3.h>
#include <GL\gl_core_4_4.h>

#include <IMGUI/imgui.h>
#include <IMGUI/imgui_impl_glfw_gl3.h>

#include <string>
#include <stdio.h>
#include <Windows.h>

#include "Utility/Gizmos.h"

#include "Application.h"
#include "State/GameStateManager.h"

Application* Application::m_instance = nullptr;

Application* Application::Instance()
{
	if (m_instance == nullptr)
	{
		m_instance = new Application();
	}
	return m_instance;
}

Application::Application()
{
	m_gameStateManager = nullptr;
}

void Application::Initialize(int _argc, char** _argv, unsigned int _width, unsigned int _height, char* _windowTitle)
{
	m_width = _width;
	m_height = _height;
	m_windowTitle = _windowTitle;

	if (glfwInit() == false)
	{
		printf("Failed to initialize GLFW\n");
		return;
	}

	m_window = glfwCreateWindow(m_width, m_height, m_windowTitle, nullptr, nullptr);

	if (m_window == nullptr)
	{
		printf("Window failed to initialize\n");
		glfwTerminate();
		return;
	}

	glfwMakeContextCurrent(m_window);

	if (ogl_LoadFunctions() == ogl_LOAD_FAILED)
	{
		printf("OGL failed to initialize\n");
		glfwTerminate();
		return;
	}

	m_gameStateManager = new GameStateManager(0);

	//If we can start with additional launch functions, let's do that.
	//for (int i = 0; i < _argc; i++)
	//{
		//std::string argv = _argv[i];

		//If we are a server
		//if (argv.find("-server") != std::string::npos)
		//{
			//GameStateManager::GetInstance()->SetState(0);
		//}
	//}
	Gizmos::create();

	ImGui_ImplGlfwGL3_Init(m_window, true);
}

void Application::Run()
{
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);	
	
	glEnable(GL_BLEND);
	glCullFace(GL_BACK);

	while (glfwWindowShouldClose(m_window) == false)
	{
		Gizmos::clear();

		glEnable(GL_DEPTH_TEST);

		ImGui_ImplGlfwGL3_NewFrame();
		ImGui::Text("%.3f ms/frame : (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
		ImGui::Separator();

		m_gameStateManager->Update();
		m_gameStateManager->Draw();

		ImGui::Render();

		glfwSwapBuffers(m_window);

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glClearColor(0.5f, 0.5f, 0.5f, 1.f);
		glfwPollEvents();

		if (ImGui::IsKeyPressed(GLFW_KEY_ESCAPE))
		{
			if (MessageBox(nullptr, TEXT("Are you sure you want to quit?"), TEXT(""), MB_YESNO) == IDYES)
			{
				glfwSetWindowShouldClose(m_window, true);
			}
		}
	}

	delete this;
}

unsigned int Application::GetWidth()
{
	return m_width;
}

unsigned int Application::GetHeight()
{
	return m_height;
}

Application::~Application()
{
	ImGui_ImplGlfwGL3_Shutdown();
	glfwDestroyWindow(m_window);
	glfwTerminate();
}