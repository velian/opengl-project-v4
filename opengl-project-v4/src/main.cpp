#define CATCH_CONFIG_RUNNER
#include "CATCH/catch.hpp"
#include "Application.h"

int main(int argc, char **argv)
{
	//int result = Catch::Session().run(argc, argv);

	Application::Instance()->Initialize(argc, argv, 1280, 720, "opengl-project-v4");
	Application::Instance()->Run();

	return 0;
}