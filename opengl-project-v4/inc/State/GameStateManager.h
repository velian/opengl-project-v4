// //
// Game State Manager Class - James Whyte
// Details		- Manages the gamestates, updating and initializing accordingly
// Last Edit	- 18/06/2015
// //

#pragma once

#include <vector>

class GameState;
class GameStateManager
{
public:

	GameStateManager();
	GameStateManager(unsigned int _initialState);

	~GameStateManager();

	void AddState(GameState* _gameState);
	void SetState(unsigned int _gameState);

	void NextState();
	void PreviousState();

	unsigned int GetState();
	unsigned int GetStateSize();

	void Update();
	void Draw();

private:

	unsigned int m_currentState, m_previousState;

	std::vector<GameState*> m_stateList;

};