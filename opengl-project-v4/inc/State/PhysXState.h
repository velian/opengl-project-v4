// //
// PhysX State Class - James Whyte
// Details			 - Made to show off PhysX
// Last Edit		 - 26/07/2015
// //

#pragma once

#include "GameState.h"

#include <GLM/glm.hpp>

#include <PHYSX/PxPhysicsAPI.h>
#include <PHYSX/PxScene.h>

#include "Entity/TriggerVolume.h"

using namespace physx;
class MemAlloc : public PxAllocatorCallback
{
public:
	virtual ~MemAlloc() {}
	virtual void* allocate(size_t size, const char* typeName, const char* filename, int line)
	{
		void* pointer = _aligned_malloc(size, 16);
		return pointer;
	}
	virtual void deallocate(void* ptr)
	{
		_aligned_free(ptr);
	}
};

class ParticleFluidEmitter;
class Camera;
class PlayerController;
class PhysXState : public GameState
{
public:

	//Constructor, called when added to the gamestatemanager
	PhysXState(unsigned int _id, GameStateManager* _gameStateManager);

	//Initializer, called when switching this to the active state
	virtual void Initialize();

	virtual void Update(double _deltaTime);
	virtual void Draw();

protected:

	virtual void DrawGUI(char* _stateName);

private:

	void AddWidget(PxShape* _shape, PxRigidActor* _actor, glm::vec4 _colour = glm::vec4(1));
	void AddPlane(PxShape* _shape, PxRigidActor* _actor, glm::vec4 _colour);
	void AddBox(PxShape* _shape, PxRigidActor* _actor, glm::vec4 _colour);
	void AddSphere(PxShape* _shape, PxRigidActor* _actor, glm::vec4 _colour);
	void AddCapsule(PxShape* _shape, PxRigidActor* _actor, glm::vec4 _colour);

	//State objects go in here
	PxSceneDesc* m_sceneDescription;

	PxFoundation* m_physicsFoundation;
	PxPhysics* m_physics;
	PxScene* m_physicsScene;
	PxDefaultErrorCallback m_defaultErrorCallback;
	PxDefaultAllocator m_defaultAllocatorCallback;
	PxSimulationFilterShader m_defaultFilterShader = PxDefaultSimulationFilterShader;
	PxMaterial* m_physicsMaterial;
	PxMaterial* m_boxMaterial;
	PxCooking* m_physicsCooker;

	//Aux Scene Data
	PxControllerManager* m_controllerManager;
	TriggerCallback m_triggerCallback;
	float m_bloodTimer;

	//Positional data
	PxTransform* m_ragdollPosition;

	//Physics Object	
	PlayerController* m_playerController;
	PxRigidStatic* m_trigger;
	PxRigidStatic* m_plane;
	PxRigidDynamic* m_box;
	PxRigidDynamic* m_sphere;
	PxArticulation* m_ragdollArticulation;
	ParticleFluidEmitter* m_emitter;

	//Containers
	std::vector<PxRigidStatic*> m_rigidStatics;
	std::vector<PxRigidDynamic*> m_rigidDynamics;
	std::vector<PxArticulation*> m_ragdolls;
	std::vector<PxParticleFluid*> m_particleSystems;

	Camera* m_camera;
};