// //
// Custom Physics State Class - James Whyte
// Details		- Made to show off my own physics engine
// Last Edit	- 18/06/2015
// //

#pragma once

#include "GameState.h"

enum ShapeID;

class PhysicsScene;
class GameStateManager;
class Camera;
class LuaScript;
class CustomPhysics : public GameState
{
public:

	//Constructor, called when added to the gamestatemanager
	CustomPhysics(unsigned int _id, GameStateManager* _gameStateManager);

	//Initializer, called when switching this to the active state
	virtual void Initialize();

	virtual void Update(double _deltaTime);
	virtual void Draw();

	//Lua functions
	void AddRigidbody(ShapeID _shapeID);

	PhysicsScene* m_physicsScene;

protected:

	virtual void DrawGUI(char* _stateName);

private:

	//State objects go in here
	
	LuaScript* m_luaMain;
	Camera* m_camera;
};