// //
// Cube Class - James Whyte
// Details		- Cube rigidbody
// Last Edit	- 17/07/2015
// //

#pragma once

#include "Entity/Rigidbody.h"

class Cube : public Rigidbody
{
public:

	Cube(glm::vec3 _position = glm::vec3(0), bool _static = false, float _extents = 1, glm::vec4 _colour = glm::vec4(1)) : m_static(_static), m_colour(_colour)
	{
		m_position = _position;
		m_extents = _extents;

		m_shapeID = ShapeID::BOX;
	}

	void virtual Debug()
	{
		Rigidbody::Debug();

		Gizmos::addAABBFilled(m_position, glm::vec3(m_extents), m_colour);
	}

	virtual void Update(glm::vec3 _gravity, float _deltaTime)
	{
		if (!m_static)
		{
			Rigidbody::Update(_gravity, _deltaTime);
		}		
	}

	void GenerateXAxis()
	{
		m_xAxis[0] = m_position.x - m_extents;
		m_xAxis[1] = m_position.x + m_extents;
	}

	void GenerateYAxis()
	{
		m_yAxis[0] = m_position.y - m_extents;
		m_yAxis[1] = m_position.y + m_extents;
	}

	void GenerateZAxis()
	{
		m_zAxis[0] = m_position.z - m_extents;
		m_zAxis[1] = m_position.z + m_extents;
	}

	float m_extents;
	glm::vec4 m_colour;
	float m_xAxis[2];
	float m_yAxis[2];
	float m_zAxis[2];
	bool m_static;
};