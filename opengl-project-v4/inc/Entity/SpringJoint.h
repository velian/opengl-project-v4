// //
// Spring Joint - James Whyte
// Details		- Connect two rigids together
// Last Edit	- 17/07/2015
// //

#pragma once

#include "Entity/PhysicsObject.h"
#include "Entity/Rigidbody.h"

class SpringJoint : public Rigidbody
{
public:

	SpringJoint(Rigidbody*, Rigidbody*, float _springCoefficient = 0.5f, float _damping = 1.f);

	Rigidbody* m_connections[2];
	float m_damping, m_restlength, m_springCoefficient;

private:

	virtual void Update(glm::vec3 _gravity, float _deltaTime);
	virtual void Debug();
	
};