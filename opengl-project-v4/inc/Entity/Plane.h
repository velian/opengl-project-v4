// //
// Plane Class - James Whyte
// Details		- Plane rigidbody. Has collision
// Last Edit	- 19/06/2015
// //

#pragma once

#include <iostream>
#include "Entity/Rigidbody.h"

class Plane : public Rigidbody
{
public:

	Plane(glm::vec3 _normal = glm::vec3(0, 1, 0), float _distance = -10);

	void virtual Debug()
	{
		Rigidbody::Debug();

		glm::vec3 centrePoint = m_normal * m_distance;
		glm::vec3 parallel = glm::vec3(m_normal.y, -m_normal.x, m_normal.z);
		glm::vec3 start = centrePoint + (parallel * 300.0f);
		glm::vec3 end = centrePoint - (parallel * 300.0f);

		Gizmos::add2DLine(start.xy, end.xy, glm::vec4(1));
	}

	glm::vec3 m_normal;
	float m_distance;
};