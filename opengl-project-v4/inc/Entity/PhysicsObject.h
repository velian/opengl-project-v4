// //
// PhysicsObject Class - James Whyte
// Details		- Physics object. Contains shape ID for Rigidbody
// Last Edit	- 19/06/2015
// //

#pragma once

#include "Utility/Gizmos.h"
#include <GLM/glm.hpp>

enum ShapeID
{
	PLANE,
	SPHERE,
	BOX,
	JOINT
};

class PhysicsObject
{
public:

	ShapeID m_shapeID;
	void virtual Update(glm::vec3 _gravity, float _timeStep) = 0;
	void virtual Debug() = 0;
	//void virtual MakeGizmo() = 0;
	//void virtual ResetPosition(){};
};