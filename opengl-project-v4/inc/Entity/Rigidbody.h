// //
// Rigidbody Class - James Whyte
// Details		- Rigidbody; actor objects derive from this
// Last Edit	- 19/06/2015
// //

#pragma once

#include "PhysicsObject.h"

class Rigidbody : public PhysicsObject
{
public:

	Rigidbody(glm::vec3 _position = glm::vec3(0), glm::vec3 _velocity = glm::vec3(0), glm::vec2 _rotation = glm::vec2(0), float _mass = 1);
	~Rigidbody();

	void virtual Update(glm::vec3 _gravity, float _deltaTime);
	void virtual Debug();

	void AddForce(glm::vec3 _force);

	glm::vec3 m_position, m_velocity;
	glm::vec2 m_rotation;
	float m_mass, m_drag, m_force;
};