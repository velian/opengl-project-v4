// //
// Sphere Class - James Whyte
// Details		- Sphere rigidbody
// Last Edit	- 19/06/2015
// //

#pragma once

#include "Entity/Rigidbody.h"

class Sphere : public Rigidbody
{
public:

	Sphere(glm::vec3 _position = glm::vec3(0), bool _static = false, float _radius = 1, glm::vec4 _colour = glm::vec4(1)) : m_radius(_radius), m_static(_static), m_colour(_colour)
	{		
		m_position = _position;
		m_shapeID = ShapeID::SPHERE;
	}

	void virtual Debug()
	{
		Rigidbody::Debug();

		Gizmos::addSphere(m_position, m_radius, 16, 16, m_colour);
	}

	virtual void Update(glm::vec3 _gravity, float _deltaTime)
	{
		if (!m_static)
		{
			Rigidbody::Update(_gravity, _deltaTime);
		}
	}

	virtual void MakeGizmo(){}

	glm::vec4 m_colour;
	float m_radius;
	bool m_static;
};