// //
// PhysicsScene Class - James Whyte
// Details		- Holds all the rigidbodies, updating according to the timestep
// Last Edit	- 19/06/2015
// //

#pragma once

#include <vector>
#include "Rigidbody.h"

class PhysicsScene
{
public:

	PhysicsScene();

	void AddRigidbody(Rigidbody*);
	void RemoveRigidbody(Rigidbody*);

	void Update(double _deltaTime);
	void Debug();

	void CheckCollisions();

	bool SphereToSphere	(PhysicsObject*, PhysicsObject*);
	bool SphereToPlane	(PhysicsObject*, PhysicsObject*);
	bool SphereToCube	(PhysicsObject*, PhysicsObject*);

	bool CubeToPlane	(PhysicsObject*, PhysicsObject*);

	float m_timestep;
	glm::vec3 m_gravity;
	std::vector<Rigidbody*> m_rigidbodies;
};