#pragma once

//class Trigger
#include <PHYSX/PxPhysicsAPI.h>

class TriggerCallback : public physx::PxSimulationEventCallback
{
public:

	TriggerCallback(){ isTriggered = false; }

	virtual void onConstraintBreak(physx::PxConstraintInfo* constraints, physx::PxU32 count) override{}
	virtual void onWake(physx::PxActor** actors, physx::PxU32 count) override{}
	virtual void onSleep(physx::PxActor** actors, physx::PxU32 count) override{}
	virtual void onContact(const physx::PxContactPairHeader& pairHeader, const physx::PxContactPair* pairs, physx::PxU32 nbPairs) override{}
	virtual void onTrigger(physx::PxTriggerPair* pairs, physx::PxU32 count) override
	{
		if (!isTriggered)
			isTriggered = true;
	}

	bool isTriggered;
};