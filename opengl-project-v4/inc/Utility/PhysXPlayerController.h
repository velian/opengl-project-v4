#pragma once

#include <PHYSX/PxPhysicsAPI.h>
using namespace physx;

class HitReport : public PxUserControllerHitReport
{
public:

	HitReport() : PxUserControllerHitReport(){}

	virtual void onShapeHit(const PxControllerShapeHit &hit) override;
	virtual void onControllerHit(const PxControllersHit &hit) override;
	virtual void onObstacleHit(const PxControllerObstacleHit &hit) override;

	PxVec3 getContactNormal(){ return m_contactNormal; }
	void clearContactNormal(){ m_contactNormal = PxVec3(0); }

	PxVec3 m_contactNormal;
};

class PlayerController
{
public:

	PlayerController()
	{
		m_controller = nullptr;
		m_movementSpeed = 5;
		m_rotationSpeed = 5;
		m_position = PxVec3(0);
		m_rotation = 0;
	}

	void Initialize(PxScene& _scene, PxCapsuleControllerDesc* _desc, PxControllerManager* _manager);
	void Update(double _deltaTime, PxVec3 _gravity);

	PxController* m_controller;
	
	bool m_grounded;

	PxVec3 m_position;
	float m_rotation;

	float m_movementSpeed;
	float m_rotationSpeed;
};