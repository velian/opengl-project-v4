// //
// LuaScript Class	- James Whyte
// Details			- Loads in a lua script file and handles function calls for said files.
// Last Edit		- 23/06/2015
// //

#pragma once

#include <LUA/lua.hpp>
#include <string>
#include <functional>
#include <Windows.h>

#if !defined LUA_VERSION_NUM
/* Lua 5.0 */
#define luaL_Reg luaL_reg
#endif

class LuaScript
{
public:

	LuaScript(char* _fileName);
	~LuaScript();	

	void RunFile();

	void RunFunction(std::string _functionName, const char* _argv...);

	void Reload();
	void RegisterLibrary(const struct luaL_Reg* _library);

	lua_State* m_luaState;

private:
	char* m_fileName;
	bool m_loaded;
	int m_time;
};