#include <GLM/glm.hpp>
#include <GLFW/glfw3.h>
#include <IMGUI/ImGui.h>

#include "State/CustomPhysics.h"
#include "Entity/PhysicsObject.h"
#include "Entity/PhysicsScene.h"

#include "Utility/SoundManager.h"
#include "Utility/Gizmos.h"

static int LuaPlaySoundI(lua_State* _luaState)
{
	std::string name = lua_tostring(_luaState, 1); // get function argument
	int loop = lua_toboolean(_luaState, 2); // get function argument
	SoundManager::Instance()->PlayOneShot(name, loop); // calling C++ function with this argument...
	return 0;
}

static int LuaDrawSquareI(lua_State* _luaState)
{
	float positionX = (float)lua_tonumber(_luaState, 1); // get function argument
	float positionY = (float)lua_tonumber(_luaState, 2);
	float positionZ = (float)lua_tonumber(_luaState, 3);
	//float extents = lua_tostring(_luaState, 3); // get function argument
	//glm::vec4 colour = lua_tostring(_luaState, 3); // get function argument
	Gizmos::addAABB(glm::vec3(positionX, positionY, positionZ), glm::vec3(1), glm::vec4(1)); // calling C++ function with this argument...
	return 0;
}

static int LuaDrawSphereI(lua_State* _luaState)
{
	float positionX = (float)lua_tonumber(_luaState, 1); // get function argument
	float positionY = (float)lua_tonumber(_luaState, 2);
	float positionZ = (float)lua_tonumber(_luaState, 3);
	Gizmos::addSphere(glm::vec3(positionX, positionY, positionZ), 1, 16, 16, glm::vec4(1)); // calling C++ function with this argument...
	return 0;
}

static int LuaKeyDownI(lua_State* _luaState)
{
	int key = (int)lua_tonumber(_luaState, 1);
	return glfwGetKey(glfwGetCurrentContext(), (int)key);
}

static int LuaLogI(lua_State* _luaState)
{
	std::string str = lua_tostring(_luaState, 1);
	ImGui::LogCustomConsole((char*)str.c_str());
	return 0;
}

static int LuaAddRigidbodyI(lua_State* _luaState)
{
	CustomPhysics* pPhysics = (CustomPhysics*)lua_touserdata(_luaState, 1);
	int eID = (int)lua_tonumber(_luaState, 2);
	pPhysics->AddRigidbody((ShapeID)eID);
	return 0;
}

static int LuaSetTimestepI(lua_State* _luaState)
{
	CustomPhysics* pPhysics = (CustomPhysics*)lua_touserdata(_luaState, 1);
	float timestep = (float)lua_tonumber(_luaState, 2);
	pPhysics->m_physicsScene->m_timestep = timestep;
	return 0;
}

static const struct luaL_Reg library[] =
{
	{ "playSound",		LuaPlaySoundI },
	{ "drawSquare",		LuaDrawSquareI },
	{ "drawSphere",		LuaDrawSphereI },
	{ "keyDown",		LuaKeyDownI },
	{ "log",			LuaLogI },
	{ "addRigidbody",	LuaAddRigidbodyI },
	{ "setTimestep",	LuaSetTimestepI },
	{ NULL, NULL }  /* sentinel */
};