#pragma once

#include "FMOD\fmod.hpp"

class Sound
{
public:

	Sound(std::string _filePath, std::string _soundName);

	std::string m_name;
	FMOD::Sound* m_sound;
};