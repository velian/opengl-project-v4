// //
// Application Class - James Whyte
// Details		- Manages the gamestate and basic init functions.
// Last Edit	- 12/06/2015
// //

#pragma once

struct GLFWwindow;
class GameStateManager;
class Application
{
public:

	void Run();

	void Initialize(int _argc, char** _argv, unsigned int _width = 1280, unsigned int _height = 720, char* _windowTitle = "Test GL");

	unsigned int GetWidth();
	unsigned int GetHeight();

	void Shutdown();

	static Application* Instance();

private:

	Application();
	~Application();
	Application(Application const&) = delete;

	static Application* m_instance;

	GLFWwindow* m_window;
	const char* m_windowTitle;
	unsigned int m_width, m_height;

	GameStateManager* m_gameStateManager;
};